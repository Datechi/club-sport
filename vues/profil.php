<?php   
    include('modules/partie1.php');
?>
 
 <div class="container card text-center mt-4">
    <h1 class="card-header">Bienvenue Toto Dupond</h1>
    <div class="card-body text-left">
        <div class="card-title">
        Vous êtes inscrit aux cours suivants :
        </div>
        <div class="card-text">
            <ul class="inscrit text-dark text-left">
                <a href="/vues/cours.php"><li><i class="fa fa-bookmark"></i> Pilates, le 10/01/2020
                à 09h00</li></a>
                <a href="/vues/cours.php"><li><i class="fa fa-bookmark"></i> Pilates, le 17/01/2020
                à 09h00</li></a>
                <a href="/vues/cours.php"><li><i class="fa fa-bookmark"></i> Yoga, le 20/01/2020
                à 10h00</li></a>
            </ul>
        </div>
    </div>
</div>

<?php
    include('modules/partie3.php');
?>
