<?php

// ceci remplace l'instruction quand on défini un namespace à la classe
use PHPUnit\Framework\TestCase;

include_once(__DIR__ ."/../models/Seance.php");
include_once(__DIR__ ."/../models/User.php");
include_once(__DIR__ ."/../models/Database.php");

final class SeanceTest extends TestCase{

    public function testCreateSeance(){
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");

        $database = new Database();

        $this->assertNotFalse($database->createSeance($seance));
    }

    public function testGetSeanceById(){
        $database = new Database();
        // Je crée et j'insert une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        //Je récupère l'id de la séance 
        $id = $database->createSeance($seance);
        $this->assertInstanceOf(Seance::class, $database->getSeanceById($id));
    }

    public function testGetSeanceByWeek(){
        $database = new Database();
        // Je crée et j'insert une séance à la date d'aujourd'hui
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // J'insère la Séance en vérifiant que ça c'est bien passé
        $this->assertNotFalse($database->createSeance($seance));
        // Je compte le nombre de séances en lui passant le numéro de la semaine courante
        $nbSeance = count($database->getSeanceByWeek(date("W")));
        echo($nbSeance);
        // Et je vérifie qu'il y a au moins une séance dans la BD
        $this->assertGreaterThan(0, $nbSeance);
    }

    public static function tearDownAfterClass(){
        $database = new Database();
        $database->deleteAllSeance();
    }
}