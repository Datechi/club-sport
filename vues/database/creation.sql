-- Script de création de la base de données
CREATE DATABASE clublambda;

-- Création d'un utilisateur admin 
-- et on lui donne tous les droits sur la base de données
-- et sur toutes les tables de la base
CREATE USER 'adminClub'@'%' IDENTIFIED BY 'D43R3riLg';
GRANT ALL PRIVILEGES ON clublambda.* TO 'adminClub'@'%';
