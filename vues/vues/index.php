<?php
    include('modules/partie1.php')
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Bienvenue chez le Club Lambda</h1>
    <div class="card-body">
        <img class="mainImage" src="/vues/assets/images/fitness.jpg">
        <p class="p-4">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
        incididunt ut labore et dolore
        maggna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
        nisi ut aliquip ex ea commodo
        cosequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
        dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cuidatat non proident, dunt in culpa qui officia deserunt
        mollit anim id est laborum.
        </p>
        <a class="btn btn-dark" href="#">Consulter le planning</a>
    </div>
</div>

<?php 
    include('modules/partie3.php');
?>