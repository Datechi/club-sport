<?php   
    include('modules/partie1.php');
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Planning de la semaine</h1>
    <div class="card-body">
        <div class="row">
            <div id="lundi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>LUNDI</h3>
                <?php
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                ?>
            </div>
            <div id="mardi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>MARDI</h3>
                <?php
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                ?>
            </div>
            <div id="mercredi" class="col-6 col-md col-lg-2 border border-primary border-top-0">
            <h3>MERCREDI</h3>
                <?php
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                ?>
            </div>
            <div id="jeudi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
            <h3>JEUDI</h3>
                <?php   
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                ?>
            </div>
            <div id="vendredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
            <h3>VENDREDI</h3>
                <?php   
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                ?>
            </div>
        

            <div id="samedi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
            <h3>SAMEDI</h3>
                <?php
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                    include('modules/etiquette.php');
                ?>
            </div>
        </div>
    </div>
</div>

<?php
    include('modules/partie3.php');
?>

