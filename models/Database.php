<?php
include_once('Seance.php');
include_once('User.php');


/**
 * Classe de connexion à la base de donnée
 */

 class Database{

    // Constantes de connexion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clublambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "D43R3riLg";

    // Atribut de la classe
    private $connexion;

    //Constructeur pour initier la connextion
    public function __construct(){
        try{
            $this->connexion = new
            PDO("mysql:host=".self::DB_HOST.";port=".self::DB_PORT.";dbname=".self::DB_NAME.";charset=UTF8",
                                    self::DB_USER,
                                    self::DB_PASSWORD);
        } catch (PDOExeption $e) {
            echo 'Connexion échouée :' . $e->getMessage();
        }
    }

    /**
     * Fonction pour créer une nouvelle séance en base de donées
     * 
     * @param{Seance} seance : la séance à sauvegarder
     * 
     * @return(integer, boolean) l'id si la séance a été créée ou false sinon
     */
    public function createSeance(Seance $seance){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur)
            VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
        );
        // J'exécute ma requête en passant les valeur de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre"             => $seance->getTitre(),
            "description"       => $seance->getDescription(),
            "heureDebut"        => $seance->getHeureDebut(),
            "date"              => $seance->getDate(),
            "duree"             => $seance->getDuree(),
            "nbParticipantsMax" => $seance->getNbParticipantsMax(),
            "couleur"           => $seance->getCouleur()
        ]);

        // Je récupère l'id créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            $id = $this->connexion->lastInsertID();
            return $id;
        }else{
            return false;
        }
    }

    /**
     * Cette fonction cherche la seance dont l'id est passé en paramètre 
     * et la retourne
     * 
     * @param{integer} id : l'id de la séance recherchée
     * 
     * @return{Seance|boolean} : un objet si la séance a été trouvée, false sinon
     */
    public function getSeanceById($id){
        //Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE id = :id"
        );
        // J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id" => $id]
        );
        // Je récupère le résultat
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;
    }

    /**
     * Fonction retourne toutes les séances de la semaine
     * 
     * @param{integer} week : le numéro de la semaine recherchée
     * 
     * @return{array} : un tableau de Seance s'il y a des séances programmées pour cette semaine
     */
    public function getSeanceByWeek($week){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM `seance` WHERE WEEKOFYEAR(date) = :week"
        );
        //J'exécute la requête en lui passant le numéro de la semaine
        $pdoStatement->execute(
            ["week" => $week]
        );
        // Je récupère les résultats
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
        return $seances;
    }

    public function deleteAllSeance(){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seance;"
        );
        $pdoStatement->execute();
    }
}

